﻿using Serilog;
using Serilog.Sinks.Unity;
using UnityEngine;

public class EntryPoint : MonoBehaviour
{
    private void Start()
    {
        var log = new LoggerConfiguration()
            .WriteTo.Unity()
            .WriteTo.File(Application.dataPath + "/AppLog.txt")
            .WriteTo.Seq("http://localhost:5341")
            .CreateLogger();

        //global logger settings
        Log.Logger = log;

        //some logs
        Log.Information("Here is the Information");
        Log.Warning("Here is warning {UserName} x", "Admin");
        Log.Error("Here is an error");

        // Important to call at exit so that batched events are flushed.
        Log.CloseAndFlush();
    }
}
