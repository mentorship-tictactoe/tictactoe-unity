﻿using System.IO;
using UnityEditor;
using UnityEngine;

namespace Tools.Protobuf.Editor
{
    public static class ProtobufContextMenu
    {
        [MenuItem("Assets/Create .proto", false, 0)]
        private static void CreateProtoFile()
        {
            if (Selection.activeObject == null) return;

            var path = AssetDatabase.GetAssetPath(Selection.activeObject);
            if (File.Exists(path)) path = Path.GetDirectoryName(path);
            if (string.IsNullOrEmpty(path)) path = "Assets/";

            var window = ScriptableObject.CreateInstance<CreateProtoFileWindow>();
            window.titleContent.text = "New .proto file";
            window.ShowUtility();
            window.ChooseFileNameEvent += (fileName, defaultNamespace) => { SaveProtoFile(path, fileName, defaultNamespace); };
        }

        private static void SaveProtoFile(string path, string fileName, string defaultNamespace)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                return;
            }

            path = Path.Combine(path, $"{fileName}.proto");

            using (StreamWriter writer = new StreamWriter(path))
            {
                writer.WriteLine("syntax = \"proto3\";");
                writer.WriteLine("");
                writer.WriteLine($"package {defaultNamespace};");
                writer.WriteLine("");
                writer.WriteLine("");
                writer.WriteLine($"message {fileName} {{}}");
            }

            AssetDatabase.Refresh();
        }
    }
}
