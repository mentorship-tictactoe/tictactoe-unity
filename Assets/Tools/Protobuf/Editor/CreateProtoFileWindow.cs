﻿using System;
using UnityEditor;
using UnityEngine;

namespace Tools.Protobuf.Editor
{
    public class CreateProtoFileWindow : EditorWindow
    {
        private const string FILE_NAME_LABEL = "File name: ";
        private const string NAMESPACE_LABEL = "Namespace: ";

        public event Action<string, string> ChooseFileNameEvent;
        private string defaultNamespace = "mentorship.protobuf";
        private string fileName = string.Empty;

        private void OnGUI()
        {
            fileName = EditorGUILayout.TextField(FILE_NAME_LABEL, fileName);
            defaultNamespace = EditorGUILayout.TextField(NAMESPACE_LABEL, defaultNamespace);
            Event e = Event.current;

            if (GUILayout.Button("Create new file") || e.keyCode == KeyCode.Return)
            {
                ChooseFileNameEvent?.Invoke(fileName, defaultNamespace);
                Close();
            }

            if (GUILayout.Button("Cancel") || e.keyCode == KeyCode.Escape)
            {
                Close();
            }
        }
    }
}
