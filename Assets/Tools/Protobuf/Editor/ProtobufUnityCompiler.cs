﻿using System.Diagnostics;
using System.IO;
using UnityEditor;
using UnityEngine;

#pragma warning disable 618

namespace Tools.Protobuf.Editor
{
    public class ProtobufUnityCompiler : AssetPostprocessor
    {

        public static readonly string PrefProtocEnable = "ProtobufUnity_Enable";
        public static readonly string PrefProtocExecutable = "ProtobufUnity_ProtocExecutable";
        public static readonly string ProtobufUnity_OutputPath = "ProtobufUnity_OutputPath";
        public static readonly string PrefLogError = "ProtobufUnity_LogError";
        public static readonly string PrefLogStandard = "ProtobufUnity_LogStandard";
        public static readonly string DefaultOutputPath = "Protocol/Generated";
        public static readonly string AssetsFolder = "Assets";
        public static readonly string ProtocolFolder = "Protocol";

        public static bool IsEnabled
        {
            get
            {
                return EditorPrefs.GetBool(PrefProtocEnable, true);
            }
            set
            {
                EditorPrefs.SetBool(PrefProtocEnable, value);
            }
        }
        public static bool IsLogError
        {
            get
            {
                return EditorPrefs.GetBool(PrefLogError, true);
            }
            set
            {
                EditorPrefs.SetBool(PrefLogError, value);
            }
        }

        public static bool IsLogStandard
        {
            get
            {
                return EditorPrefs.GetBool(PrefLogStandard, false);
            }
            set
            {
                EditorPrefs.SetBool(PrefLogStandard, value);
            }
        }

        public static string RawExcPath
        {
            get
            {
                return EditorPrefs.GetString(PrefProtocExecutable, "");
            }
            set
            {
                EditorPrefs.SetString(PrefProtocExecutable, value);
            }
        }

        public static string ExcPath
        {
            get
            {
                string ret = EditorPrefs.GetString(PrefProtocExecutable, "");
                if (ret.StartsWith(".."))
                {
                    return Path.Combine(Application.dataPath, ret);
                }

                return ret;
            }
            set
            {
                EditorPrefs.SetString(PrefProtocExecutable, value);
            }
        }

        public static string RawOutputPath
        {
            get
            {
                return EditorPrefs.GetString(ProtobufUnity_OutputPath, DefaultOutputPath);
            }
            set
            {
                EditorPrefs.SetString(ProtobufUnity_OutputPath, value);
            }
        }

        public static string OutputPath
        {
            get
            {
                string ret = EditorPrefs.GetString(ProtobufUnity_OutputPath, DefaultOutputPath);
                var pathParts = ret.Split('/');

                return Path.Combine(pathParts);
            }
            set
            {
                EditorPrefs.SetString(ProtobufUnity_OutputPath, value);
            }
        }

        [PreferenceItem("Protobuf")]
        private static void PreferencesItem()
        {
            EditorGUI.BeginChangeCheck();

            IsEnabled = EditorGUILayout.Toggle(new GUIContent("Enable Protobuf Compilation", ""), IsEnabled);

            EditorGUI.BeginDisabledGroup(!IsEnabled);

            EditorGUILayout.HelpBox(@"On Windows put the path to protoc.exe (e.g. C:\My Dir\protoc.exe), on macOS and Linux you can use ""which protoc"" to find its location. (e.g. /usr/local/bin/protoc)", MessageType.Info);

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Path to protoc", GUILayout.Width(100));
            RawExcPath = EditorGUILayout.TextField(RawExcPath, GUILayout.ExpandWidth(true));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.BeginHorizontal();
            EditorGUILayout.LabelField("Relative output path from Assets forlder:(/-folder delimiter)", GUILayout.Width(100));
            RawOutputPath = EditorGUILayout.TextField(RawOutputPath, GUILayout.ExpandWidth(true));
            EditorGUILayout.EndHorizontal();

            EditorGUILayout.Space();

            IsLogError = EditorGUILayout.Toggle(new GUIContent("Log Error Output", "Log compilation errors from protoc command."), IsLogError);

            IsLogStandard = EditorGUILayout.Toggle(new GUIContent("Log Standard Output", "Log compilation completion messages."), IsLogStandard);

            EditorGUILayout.Space();

            if (GUILayout.Button(new GUIContent("Force Compilation")))
            {
                CompileAllInProject();
            }

            EditorGUI.EndDisabledGroup();

            if (EditorGUI.EndChangeCheck())
            {
            }
        }

        private static string[] AllProtoFiles
        {
            get
            {
                string[] protoFiles = Directory.GetFiles(Application.dataPath, "*.proto", SearchOption.AllDirectories);
                return protoFiles;
            }
        }

        private static string[] IncludePaths
        {
            get
            {
                string[] protoFiles = AllProtoFiles;

                string[] includePaths = new string[protoFiles.Length];
                for (int i = 0; i < protoFiles.Length; i++)
                {
                    string protoFolder = Path.GetDirectoryName(protoFiles[i]);
                    includePaths[i] = protoFolder;
                }
                return includePaths;
            }
        }

        private static bool isAnyChanges = false;

        private static void OnPostprocessAllAssets(string[] importedAssets, string[] deletedAssets, string[] movedAssets, string[] movedFromAssetPaths)
        {
            isAnyChanges = false;
            if (IsEnabled == false)
            {
                return;
            }


            foreach (string str in importedAssets)
            {
                if (CompileProtobufAssetPath(str, IncludePaths) == true)
                {
                    isAnyChanges = true;
                }
            }

            if (isAnyChanges)
            {
                UnityEngine.Debug.Log(nameof(ProtobufUnityCompiler));
                AssetDatabase.Refresh();
            }
        }

        private static void CompileAllInProject()
        {
            if (IsLogStandard)
            {
                UnityEngine.Debug.Log("Protobuf Unity : Compiling all .proto files in the project...");
            }


            foreach (string s in AllProtoFiles)
            {
                if (IsLogStandard)
                {
                    UnityEngine.Debug.Log("Protobuf Unity : Compiling " + s);
                }

                string assetPath = s.Substring(s.IndexOf("Assets"));
                CompileProtobufSystemPath(s, assetPath, IncludePaths);
            }
            UnityEngine.Debug.Log(nameof(ProtobufUnityCompiler));
            AssetDatabase.Refresh();
        }

        private static bool CompileProtobufAssetPath(string assetPath, string[] includePaths)
        {
            string protoFileSystemPath = Directory.GetParent(Application.dataPath) + Path.DirectorySeparatorChar.ToString() + assetPath;
            return CompileProtobufSystemPath(protoFileSystemPath, assetPath, includePaths);
        }


        private static bool CompileProtobufSystemPath(string protoFileSystemPath, string assetPath, string[] includePaths)
        {
            if (Path.GetExtension(protoFileSystemPath) == ".proto")
            {
                string outputPath = GetOutputPath(assetPath);
                Directory.CreateDirectory(outputPath);
                string options = " --csharp_out {0} ";


                /*foreach (string s in includePaths)
                {
                    options += $" --proto_path {s} ";
                }*/

                string protoRootPath = Application.dataPath;
                options += $" --proto_path {protoRootPath} ";

                string finalArguments = $"\"{protoFileSystemPath}\"" + string.Format(options, outputPath);

                ProcessStartInfo startInfo = new ProcessStartInfo() { FileName = ExcPath, Arguments = finalArguments };

                Process proc = new Process() { StartInfo = startInfo };
                proc.StartInfo.UseShellExecute = false;
                proc.StartInfo.RedirectStandardOutput = true;
                proc.StartInfo.RedirectStandardError = true;
                proc.Start();

                string output = proc.StandardOutput.ReadToEnd();
                string error = proc.StandardError.ReadToEnd();
                proc.WaitForExit();

                if (IsLogStandard)
                {
                    if (output != "")
                    {
                        UnityEngine.Debug.Log("Protobuf Unity : " + output);
                    }
                    UnityEngine.Debug.Log("Protobuf Unity : Compiled " + Path.GetFileName(protoFileSystemPath));
                }

                if (IsLogError && error != "")
                {
                    UnityEngine.Debug.LogError("Protobuf Unity : " + error);
                }
                return true;
            }
            return false;
        }

        private static string GetOutputPath(string assetPath)
        {
            string outputFolder = Path.GetDirectoryName(assetPath);
            int assetsFolderLength = AssetsFolder.Length + 1;
            string relativeOutputPath = outputFolder.Substring(outputFolder.IndexOf(AssetsFolder, assetsFolderLength) + assetsFolderLength);

            int protocolFolderLength = ProtocolFolder.Length + 3;

            string excludeProtocol = relativeOutputPath.Substring(relativeOutputPath.IndexOf(ProtocolFolder, protocolFolderLength) + protocolFolderLength);
            string outputFileSystemPath = Path.Combine(Application.dataPath, OutputPath, excludeProtocol);
            return outputFileSystemPath;
        }
    }
}